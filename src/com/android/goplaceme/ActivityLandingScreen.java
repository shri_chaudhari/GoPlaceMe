package com.android.goplaceme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.android.goplaceme.utils.LocationTracker;

/**
 * @author Shri
 * This is the landing screen , where I going to show categories as well facility to user to select radius
 *
 */
public class ActivityLandingScreen extends Activity implements OnClickListener {

	private LocationTracker locationTracker;

	public static Double dbl_latitude;
	public static Double dbl_longitude;

	private SeekBar seekBar_radius;

	private TextView txtView_radius;

	private LinearLayout foodLayout;
	private LinearLayout schoolLayout;
	private LinearLayout gymLayout;
	private LinearLayout hospitalLayout;
	private LinearLayout restLayout;
	private LinearLayout spaLayout;

	private String str_category;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_landing_screen);

		initComponent();
	}

	@Override
	protected void onResume()
	{
		super.onResume();	
	}

	/**
	 * This function will initialize the Component
	 */
	private void initComponent() {

		txtView_radius = (TextView)findViewById(R.id.textViewRadius);
		txtView_radius.setText("2500");
		seekBar_radius = (SeekBar)findViewById(R.id.seekBarDistance);
		seekBar_radius.setProgress(2500);		
		seekBar_radius.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
			}

			@Override
			public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
				txtView_radius.setText(""+progress);
			}
		});

		// Categories layout
		foodLayout = (LinearLayout)findViewById(R.id.foodContainer);
		foodLayout.setOnClickListener(this);
		schoolLayout = (LinearLayout)findViewById(R.id.schoolContainer);
		schoolLayout.setOnClickListener(this);
		gymLayout = (LinearLayout)findViewById(R.id.gymContainer);
		gymLayout.setOnClickListener(this);
		hospitalLayout = (LinearLayout)findViewById(R.id.hospitalContainer);
		hospitalLayout.setOnClickListener(this);
		restLayout = (LinearLayout)findViewById(R.id.restContainer);
		restLayout.setOnClickListener(this);
		spaLayout = (LinearLayout)findViewById(R.id.spaContainer);
		spaLayout.setOnClickListener(this);

		locationTracker = new LocationTracker(this);
		// check if user location can get else redirect user to setting page
		if (locationTracker.canGetLocation()) 
		{
			dbl_latitude = locationTracker.getLatitude();
			dbl_longitude = locationTracker.getLongitude();
		} 
		else 
		{
			locationTracker.showSettingsAlert();
			return;
		}

	}

	/**
	 * This function will redirect you to the list places activity
	 */
	private void callListPlacesActivity() {

		Intent intent_listplaces = new Intent(ActivityLandingScreen.this, ActivityPlaceListScreen.class);
		intent_listplaces.putExtra("user_latitude", dbl_latitude);
		intent_listplaces.putExtra("user_longitude", dbl_longitude);
		intent_listplaces.putExtra("radius", Double.parseDouble(txtView_radius.getText().toString()));
		intent_listplaces.putExtra("type", str_category);
		startActivity(intent_listplaces);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.menu_favorite:

			// Calling Favorite list activity
			Intent intent_favorite = new Intent(ActivityLandingScreen.this, ActivityMyFavoritePlacesScreen.class);
			startActivity(intent_favorite);			
			break;		
		}
		return true;
	}

	@Override
	public void onClick(View view) {

		
		int id = view.getId();
		if (id == R.id.foodContainer) {
			str_category = "food";
		} else if (id == R.id.schoolContainer) {
			str_category = "school";
		} else if (id == R.id.gymContainer) {
			str_category = "gym";
		} else if (id == R.id.hospitalContainer) {
			str_category = "hospital";
		} else if (id == R.id.restContainer) {
			str_category = "restaurant";
		} else if (id == R.id.spaContainer) {
			str_category = "spa";
		}
		// check if user location can get else redirect user to setting page
		if (locationTracker.canGetLocation()) 
		{
			dbl_latitude = locationTracker.getLatitude();
			dbl_longitude = locationTracker.getLongitude();
			callListPlacesActivity();
		} 
	}
}


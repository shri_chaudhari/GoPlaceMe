package com.android.goplaceme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 *  @author Shri
 *  
 *  This is the simple splash screen activity which will splash for 5 second and then will 
 *  goto Landing screen    
 *
 */
public class ActivitySplashScreen extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);

		// Wait for 5 second and then calling landing screen
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {

				// Creating landing screen intent
				Intent intent_landingScreen = new Intent(ActivitySplashScreen.this, ActivityLandingScreen.class);
				startActivity(intent_landingScreen);

				// Close this activity
				finish();
			}
		}, 5*1000); // wait for 5 seconds

	}

}

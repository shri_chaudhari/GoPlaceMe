package com.android.goplaceme;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.goplaceme.google.GooglePlaces;
import com.android.goplaceme.google.PlaceDetails;
import com.android.goplaceme.imagecache.ImageLoader;
import com.android.goplaceme.model.PlaceModel;
import com.android.goplaceme.utils.AlertDialogManager;
import com.android.goplaceme.utils.ConnectionDetector;
import com.android.goplaceme.utils.Constants;

public class ActivityPlaceDetailsScreen extends Activity {


	private boolean isInternetPresent = false;
	private boolean isFavPlace = false;

	private ConnectionDetector cd;

	private AlertDialogManager alert = new AlertDialogManager();

	private GooglePlaces googlePlaces;

	private PlaceDetails placeDetails;

	private ProgressDialog pDialog;

	private Button btn_showOnMap;
	private Button btn_addToFav;

	private Double dbl_latitude;
	private Double dbl_longitude;
	private Double place_latitude;
	private Double place_longitude;

	private TextView lbl_name;
	private TextView lbl_address;
	private TextView lbl_location;

	private ImageView imgView_banner;	

	private String reference;
	private String str_name;
	private String str_address;
	private String image_url;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_place_details);

		Intent intent = getIntent();
		
		// Place referece id
		reference = intent.getStringExtra("reference");
		dbl_latitude = intent.getDoubleExtra("user_latitude", 0.0);
		dbl_longitude = intent.getDoubleExtra("user_longitude", 0.0);
		str_name = intent.getStringExtra("place_name");
		str_address = intent.getStringExtra("place_address");
		place_latitude = intent.getDoubleExtra("place_latitude",0.0);
		place_longitude = intent.getDoubleExtra("place_longitude", 0.0);

		
		// Displaying all the details in the view
		// single_place.xml
		lbl_name = (TextView) findViewById(R.id.name);
		lbl_address = (TextView) findViewById(R.id.address);
		lbl_location = (TextView) findViewById(R.id.location);
		imgView_banner = (ImageView)findViewById(R.id.imgViewBanner);

		if(str_name != null)
			lbl_name.setText(str_name);
		if(str_address != null)
			lbl_address.setText(str_address);
		if(place_latitude != null && place_longitude != null)
			lbl_location.setText(Html.fromHtml("<b>Latitude:</b> " + place_latitude + ", <b>Longitude:</b> " + place_longitude));


		btn_showOnMap = (Button)findViewById(R.id.btn_showOnMap);
		btn_showOnMap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				Intent intent_map = new Intent(getApplicationContext(),
						ActivityMapScreen.class);
				// Sending user current geo location
				intent_map.putExtra("user_latitude", dbl_latitude);
				intent_map.putExtra("user_longitude", dbl_longitude);
				intent_map.putExtra("Activity","ActivityPlaceDetails");
				intent_map.putExtra("place_latitude",place_latitude);
				intent_map.putExtra("place_longitude", place_longitude);
				intent_map.putExtra("place_name",str_name);


				startActivity(intent_map);


			}
		});

		btn_addToFav = (Button)findViewById(R.id.btn_AddToFav);
		btn_addToFav.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				if(isFavPlace)
				{
					isFavPlace = false;
					btn_addToFav.setText(getResources().getString(R.string.label_add_fav));
					
					PlaceModel placeModel = new PlaceModel();
					placeModel.deletePlaceByReference(reference, getApplicationContext());
					Toast.makeText(ActivityPlaceDetailsScreen.this,getResources().getString(R.string.msg_removed_fav), Toast.LENGTH_LONG).show();
					
				}
				else
				{
					PlaceModel objPlaceModel = new PlaceModel(reference, str_name,str_address,String.valueOf(dbl_latitude), String.valueOf(dbl_longitude),image_url);
					if(objPlaceModel.insertPlace(getApplicationContext()))
						Toast.makeText(ActivityPlaceDetailsScreen.this,getResources().getString(R.string.msg_added_fav),Toast.LENGTH_LONG).show();
					isFavPlace = true;
					btn_addToFav.setText(getResources().getString(R.string.label_remove_fav));
				}
				
			}
		});
		
		checkEntryInDb();
		
		if(isFavPlace)
			btn_addToFav.setText(getResources().getString(R.string.label_remove_fav));
		else
			btn_addToFav.setText(getResources().getString(R.string.label_add_fav));
	}

	/**
	 * This function will check place in db and show it from db if it is available else
	 * load it from server
	 */
	private void checkEntryInDb() {
		
		PlaceModel placeModel = new PlaceModel();		
		placeModel = placeModel.getPlaceByReference(getApplicationContext(), reference);
		if(placeModel != null)
		{
			isFavPlace = true;
			str_name = placeModel.name == null ? "Not available":placeModel.name;
			lbl_name.setText(str_name);
			str_address = placeModel.address == null ? "Not available":placeModel.address;
			lbl_address.setText(str_address);	
			place_latitude = Double.parseDouble(placeModel.latitude);
			place_longitude = Double.parseDouble(placeModel.longitude);
			lbl_location.setText(Html.fromHtml("<b>Latitude:</b> " + placeModel.latitude + ", <b>Longitude:</b> " + placeModel.longitude));

			if(placeModel.photoURL !=null && placeModel.photoURL.length() != 0)
			{
				image_url = placeModel.photoURL;
				int loader = R.drawable.ic_default;
				ImageLoader imgLoader = new ImageLoader(getApplicationContext());				
				imgLoader.DisplayImage(placeModel.photoURL, loader, imgView_banner);
			}
			
		}
		else
			new LoadSinglePlaceDetails().execute(reference);

		
	}


	/**
	 * Background Async Task to Load Google places
	 * */
	class LoadSinglePlaceDetails extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ActivityPlaceDetailsScreen.this);
			pDialog.setMessage("Loading profile ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting Profile JSON
		 * */
		@Override
		protected String doInBackground(String... args) {
			String reference = args[0];

			// creating Places class object
			googlePlaces = new GooglePlaces();

			// Check if used is connected to Internet
			try {
				placeDetails = googlePlaces.getPlaceDetails(reference);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					
					if(placeDetails != null){
						String status = placeDetails.status;

						if(status.equals("OK")){
							if (placeDetails.result != null) {

								String name = placeDetails.result.name;
								String address = placeDetails.result.formatted_address;
								String phone = placeDetails.result.formatted_phone_number;
								String latitude = Double.toString(placeDetails.result.geometry.location.lat);
								String longitude = Double.toString(placeDetails.result.geometry.location.lng);



								// Check for null data from google
								// Sometimes place details might missing
								name = name == null ? "Not present" : name; // if name is null display as "Not present"
								address = address == null ? "Not present" : address;
								phone = phone == null ? "Not present" : phone;
								latitude = latitude == null ? "Not present" : latitude;
								longitude = longitude == null ? "Not present" : longitude;


								lbl_address.setText(address);	
								str_address = address;
								lbl_location.setText(Html.fromHtml("<b>Latitude:</b> " + latitude + ", <b>Longitude:</b> " + longitude));

								if(placeDetails.result.photos !=null && placeDetails.result.photos.size() != 0)
								{
									int loader = R.drawable.ic_default;
									ImageLoader imgLoader = new ImageLoader(getApplicationContext());
									image_url =Constants.PLACE_PHOTO_URL+"key="+Constants.API_KEY+
											"&maxwidth=1024&photoreference="+placeDetails.result.photos.get(0).photo_reference;

									imgLoader.DisplayImage(image_url, loader, imgView_banner);
								}

							}
						}
						else if(status.equals("ZERO_RESULTS")){
							alert.showAlertDialog(ActivityPlaceDetailsScreen.this, getResources().getString(R.string.title_near_places),
									getResources().getString(R.string.msg_no_places_found),
									false);
						}
						else if(status.equals("UNKNOWN_ERROR"))
						{
							alert.showAlertDialog(ActivityPlaceDetailsScreen.this, "Places Error",
									getResources().getString(R.string.msg_unkown_error),
									false);
						}
						else if(status.equals("OVER_QUERY_LIMIT"))
						{
							alert.showAlertDialog(ActivityPlaceDetailsScreen.this, "Places Error",
									getResources().getString(R.string.msg_query_limit),
									false);
						}
						else if(status.equals("REQUEST_DENIED"))
						{
							alert.showAlertDialog(ActivityPlaceDetailsScreen.this, "Places Error",
									getResources().getString(R.string.msg_request_denied),
									false);
						}
						else if(status.equals("INVALID_REQUEST"))
						{
							alert.showAlertDialog(ActivityPlaceDetailsScreen.this, "Places Error",
									getResources().getString(R.string.msg_invalid_request),
									false);
						}
						else
						{
							alert.showAlertDialog(ActivityPlaceDetailsScreen.this, "Places Error",
									getResources().getString(R.string.msg_error),
									false);
						}
					}else{
						alert.showAlertDialog(ActivityPlaceDetailsScreen.this, "Places Error",
								getResources().getString(R.string.msg_unable_fetch_details),
								false);
					}


				}
			});

		}

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.menu_favorite:
			
			Intent intent_favorite = new Intent(ActivityPlaceDetailsScreen.this, ActivityMyFavoritePlacesScreen.class);
			startActivity(intent_favorite);
			
			break;		
		}
		return true;
	}


}

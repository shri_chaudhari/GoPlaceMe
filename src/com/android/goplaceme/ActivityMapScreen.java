package com.android.goplaceme;

import java.net.URL;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.goplaceme.google.Place;
import com.android.goplaceme.google.PlacesList;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * @author Shri
 * This class will responsible for showing maps on screen
 *
 */

@SuppressLint("NewApi")
public class ActivityMapScreen extends Activity {


	private PlacesList nearPlaces;	

	private Button btn_showOnList;

	private double latitude;
	private double longitude;
	private double place_lat;
	private double place_long;

	private String str_activity;
	private String str_placeName;

	private GoogleMap mMap;

	private HashMap<Marker, Place> mMarkersHashMap;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);

		// Getting intent data
		Intent intent_data = getIntent();
		str_activity = intent_data.getStringExtra("Activity");
		latitude = intent_data.getDoubleExtra("user_latitude",0.0);
		longitude = intent_data.getDoubleExtra("user_longitude",0.0);

		if(str_activity.equalsIgnoreCase("ActivityPlacesList"))
		{
			nearPlaces = (PlacesList) intent_data.getSerializableExtra("near_places");
		}
		else
		{
			place_lat = intent_data.getDoubleExtra("place_latitude", 0.0);
			place_long = intent_data.getDoubleExtra("place_longitude", 0.0);
			str_placeName = intent_data.getStringExtra("place_name");
		}

		//Initializing button
		btn_showOnList = (Button)findViewById(R.id.btn_show_list);
		btn_showOnList.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg) {

				ActivityMapScreen.this.finish();				
			}
		});

		// Initialize the HashMap for Markers and MyMarker object
		mMarkersHashMap = new HashMap<Marker, Place>();

		setUpMap();

		try {
			// Create user marker with custom icon and other options
			MarkerOptions markerOption = new MarkerOptions().position(new LatLng(latitude,longitude));
			markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map));
			Marker currentMarker = mMap.addMarker(markerOption);
			Place place_me = new Place();
			place_me.name = this.getResources().getString(R.string.msg_user_marker);
			mMarkersHashMap.put(currentMarker,place_me );
			mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
			LatLng toCenter = new LatLng(latitude,longitude);
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(toCenter, 15));

			if(nearPlaces != null && str_activity.equalsIgnoreCase("ActivityPlacesList"))
				plotMarkers(nearPlaces);
			else
			{
				btn_showOnList.setVisibility(View.GONE);
				// Create user marker with custom icon and other options
				markerOption = new MarkerOptions().position(new LatLng(place_lat,place_long));
				markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.currentlocation_icon));
				currentMarker = mMap.addMarker(markerOption);
				Place place = new Place();
				place.name = str_placeName;
				mMarkersHashMap.put(currentMarker,place );
				mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());	
				toCenter = new LatLng(place_lat,place_long);
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(toCenter, 15));
			}			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	/**
	 * This function will plot markers for all places
	 * @param markers
	 */
	private void plotMarkers(PlacesList markers)
	{
		if(markers.results.size() > 0)
		{
			for (Place myMarker : markers.results)
			{

				// Create user marker with custom icon and other options
				MarkerOptions markerOption = new MarkerOptions().position(new LatLng(myMarker.geometry.location.lat, myMarker.geometry.location.lng));
				markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.currentlocation_icon));

				Marker currentMarker = mMap.addMarker(markerOption);
				mMarkersHashMap.put(currentMarker, myMarker);

				mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
			}
		}
	}

	/*
	 * THis function will setup Map
	 */
	private void setUpMap()
	{
		// Do a null check to confirm that we have not already instantiated the map.
		if (mMap == null)
		{
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

			// Check if we were successful in obtaining the map.

			if (mMap != null)
			{
				mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
				{
					@Override
					public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker)
					{
						marker.showInfoWindow();
						return true;
					}
				});
			}
			else
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_map_uncreate), Toast.LENGTH_SHORT).show();
		}
	}

	public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter
	{
		public MarkerInfoWindowAdapter()
		{

		}

		@Override
		public View getInfoWindow(Marker marker)
		{
			return null;
		}

		@Override
		public View getInfoContents(Marker marker)
		{
			View view  = getLayoutInflater().inflate(R.layout.layout_map_info, null);

			final Place myMarker = mMarkersHashMap.get(marker);

			ImageView markerIcon = (ImageView) view.findViewById(R.id.marker_icon);

			TextView markerLabel = (TextView)view.findViewById(R.id.marker_label);		

			try {
				Log.v("Places","URL "+myMarker.icon);
				URL newurl = new URL(myMarker.icon); 
				Bitmap mIcon_val = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
				markerIcon.setImageBitmap(mIcon_val);

			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			markerIcon.setImageResource(R.drawable.ic_launcher);
			markerLabel.setText(myMarker.name);

			return view;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.menu_favorite:

			Intent intent_favorite = new Intent(ActivityMapScreen.this, ActivityMyFavoritePlacesScreen.class);
			startActivity(intent_favorite);			
			break;		
		}
		return true;
	}




}


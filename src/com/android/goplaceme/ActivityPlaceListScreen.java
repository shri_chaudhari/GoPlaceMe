package com.android.goplaceme;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.goplaceme.google.GooglePlaces;
import com.android.goplaceme.google.Place;
import com.android.goplaceme.google.PlacesList;
import com.android.goplaceme.utils.AlertDialogManager;
import com.android.goplaceme.utils.ConnectionDetector;


public class ActivityPlaceListScreen extends Activity {


	private Boolean isInternetPresent = false;

	private ConnectionDetector cd;

	private AlertDialogManager alert = new AlertDialogManager();

	private GooglePlaces googlePlaces;

	private PlacesList nearPlaces;

	private Button btnShowOnMap;

	private ProgressDialog pDialog;

	private ListView lstView_places;

	private double dbl_latitude;
	private double dbl_longitude;
	private double dbl_radius;

	private String str_type;

	// ListItems data
	ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String,String>>();


	// KEY Strings
	public static String KEY_REFERENCE = "reference"; 
	public static String KEY_NAME = "name"; 
	public static String KEY_VICINITY = "vicinity"; 

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_place_list);

		// Getting intent data
		Intent intent = getIntent();

		dbl_latitude = intent.getDoubleExtra("user_latitude",0.0);
		dbl_longitude = intent.getDoubleExtra("user_longitude",0.0);
		dbl_radius = intent.getDoubleExtra("radius", 2500.0);
		str_type = intent.getStringExtra("type");

		cd = new ConnectionDetector(getApplicationContext());
		// Check if Internet present
		isInternetPresent = cd.isConnectingToInternet();
		if (!isInternetPresent) {
			// Internet Connection is not present
			alert.showAlertDialog(ActivityPlaceListScreen.this, "Internet Connection Error",
					getResources().getString(R.string.msg_no_internet), false);

			return;
		}


		// Getting listview
		lstView_places = (ListView) findViewById(R.id.list);

		// button show on map
		btnShowOnMap = (Button) findViewById(R.id.btn_show_map);

		new LoadPlaces().execute();

		/** Button click event for shown on map */
		btnShowOnMap.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent_map = new Intent(getApplicationContext(),
						ActivityMapScreen.class);
				// Sending user current geo location
				intent_map.putExtra("Activity","ActivityPlacesList");
				intent_map.putExtra("user_latitude", dbl_latitude);
				intent_map.putExtra("user_longitude",dbl_longitude);

				// passing near places to map activity
				intent_map.putExtra("near_places", nearPlaces);
				// staring activity
				startActivity(intent_map);
			}
		});


		/**
		 * ListItem click event
		 * On selecting a listitem SinglePlaceActivity is launched
		 * */
		lstView_places.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				Place objPlace = nearPlaces.results.get(position);
				String reference = objPlace.reference;

				// Starting new intent
				Intent intent_details = new Intent(getApplicationContext(),
						ActivityPlaceDetailsScreen.class);

				intent_details.putExtra(KEY_REFERENCE, reference);
				intent_details.putExtra("user_latitude", dbl_latitude);
				intent_details.putExtra("user_longitude", dbl_longitude);
				intent_details.putExtra("place_name",objPlace.name == null ? "Not present" : objPlace.name );
				intent_details.putExtra("place_address", objPlace.vicinity == null ? "Not present" : objPlace.vicinity);				
				intent_details.putExtra("place_latitude", objPlace.geometry.location.lat);
				intent_details.putExtra("place_longitude", objPlace.geometry.location.lng);				

				startActivity(intent_details);
			}
		});
	}

	/**
	 * Background Async Task to Load Google places
	 * */
	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ActivityPlaceListScreen.this);
			pDialog.setMessage(Html.fromHtml("<b>Search</b><br/>Loading Places..."));
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting Places JSON
		 * */
		@Override
		protected String doInBackground(String... args) {
			// creating Places class object
			googlePlaces = new GooglePlaces();

			try {

				String types = str_type;		
				double radius = dbl_radius;
				// get nearest places
				nearPlaces = googlePlaces.search(dbl_latitude,
						dbl_longitude, radius, types);


			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * and show the data in UI
		 * Always use runOnUiThread(new Runnable()) to update UI from background
		 * thread, otherwise you will get error
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// Get json response status

					if(nearPlaces != null)
					{
						String status = nearPlaces.status;

						// Check for all possible status
						if(status.equals("OK")){
							// Successfully got places details
							if (nearPlaces.results != null) {
								// loop through each place
								for (Place p : nearPlaces.results) {
									HashMap<String, String> map = new HashMap<String, String>();

									// Place reference won't display in listview - it will be hidden
									// Place reference is used to get "place full details"
									map.put(KEY_REFERENCE, p.reference);

									// Place name
									map.put(KEY_NAME, p.name);


									// adding HashMap to ArrayList
									placesListItems.add(map);
								}
								// list adapter
								ListAdapter adapter = new SimpleAdapter(ActivityPlaceListScreen.this, placesListItems,
										R.layout.item_list,
										new String[] { KEY_REFERENCE, KEY_NAME}, new int[] {
										R.id.reference, R.id.name });

								// Adding data into listview
								lstView_places.setAdapter(adapter);
							}
						}
						else if(status.equals("ZERO_RESULTS")){
							// Zero results found
							alert.showAlertDialog(ActivityPlaceListScreen.this, "Near Places",
									getResources().getString(R.string.msg_no_places_found),
									false);
						}
						else if(status.equals("UNKNOWN_ERROR"))
						{
							alert.showAlertDialog(ActivityPlaceListScreen.this, "Places Error",
									getResources().getString(R.string.msg_unkown_error),
									false);
						}
						else if(status.equals("OVER_QUERY_LIMIT"))
						{
							alert.showAlertDialog(ActivityPlaceListScreen.this, "Places Error",
									getResources().getString(R.string.msg_query_limit),
									false);
						}
						else if(status.equals("REQUEST_DENIED"))
						{
							alert.showAlertDialog(ActivityPlaceListScreen.this, "Places Error",
									getResources().getString(R.string.msg_request_denied),
									false);
						}
						else if(status.equals("INVALID_REQUEST"))
						{
							alert.showAlertDialog(ActivityPlaceListScreen.this, "Places Error",
									getResources().getString(R.string.msg_invalid_request),
									false);
						}
						else
						{
							alert.showAlertDialog(ActivityPlaceListScreen.this, "Places Error",
									getResources().getString(R.string.msg_error),
									false);
						}

					}
					else
						alert.showAlertDialog(ActivityPlaceListScreen.this, "Places Error",
								getResources().getString(R.string.msg_no_internet),
								false);

				}
			});

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.menu_favorite:

			Intent intent_favorite = new Intent(ActivityPlaceListScreen.this, ActivityMyFavoritePlacesScreen.class);
			startActivity(intent_favorite);

			break;		
		}
		return true;
	}




}

package com.android.goplaceme.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "GooglePlaces";

	// Table Names
	public static final String TABLE_PLACES = "tblplaces";


	// Table Create Statements
	// Todo table create statement
	private static final String CREATE_TABLE_PLACES = "CREATE TABLE "+
			TABLE_PLACES + "(id INTEGER PRIMARY KEY," +
			"reference TEXT,"+
			"name TEXT," + 
			"address TEXT," +
			"latitude TEXT," +
			"longitude TEXT,"+
			"photoURL TEXT,"+
			"createdAt DATETIME)";



	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		// creating required tables
		db.execSQL(CREATE_TABLE_PLACES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// on upgrade drop older tables
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLACES);

		// create new tables
		onCreate(db);
	}

	// closing database
	public void closeDB() {
		SQLiteDatabase db = this.getReadableDatabase();
		if (db != null && db.isOpen())
			db.close();
	}
}
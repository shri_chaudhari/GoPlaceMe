package com.android.goplaceme.utils;

public class Constants {

	// Google API Key
	public static final String API_KEY = "AIzaSyArKSl9nU-Xm303Dua94oB_1mgpobzQWto"; // place your API key here

	// Google Places serach url's
	public static final String PLACES_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/search/json?";
	public static final String PLACES_TEXT_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/search/json?";
	public static final String PLACES_DETAILS_URL = "https://maps.googleapis.com/maps/api/place/details/json?";
	public static final String PLACE_PHOTO_URL = "https://maps.googleapis.com/maps/api/place/photo?";

}

package com.android.goplaceme.google;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.api.client.util.Key;

public class Place implements Serializable {

	@Key
	public String id;
	
	@Key
	public String name;
	
	@Key
	public String reference;
	
	@Key
	public String icon;
	
	@Key
	public String vicinity;
	
	@Key
	public Geometry geometry;
	
	@Key
	public ArrayList<Photos> photos;
	
	@Key
	public String formatted_address;
	
	@Key
	public String formatted_phone_number;

	@Override
	public String toString() {
		return name + " - " + id + " - " + reference;
	}
	
	public static class Photos implements Serializable
	{
		@Key
		public String photo_reference;
	}
	
	public static class Photo implements Serializable
	{
		@Key
		public String photo_reference;
		
	}
	
	public static class Geometry implements Serializable
	{
		@Key
		public Location location;
	}
	
	public static class Location implements Serializable
	{
		@Key
		public double lat;
		
		@Key
		public double lng;
	}
	
}

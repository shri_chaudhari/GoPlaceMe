package com.android.goplaceme.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.android.goplaceme.utils.DatabaseHelper;

public class PlaceModel {

	public int id;
	public String reference;
	public String name;
	public String address;
	public String latitude;
	public String longitude;
	public String photoURL;


	public PlaceModel() {

	}

	public PlaceModel(String reference,String name,String address,String latitude, String longitude,String PhotoURL)
	{		
		this.reference = reference;
		this.name = name;
		this.address = address;
		this.latitude = latitude;
		this.longitude = longitude;
		this.photoURL = PhotoURL;
	}

	/**
	 * This function will insert data into the places table
	 */
	public boolean insertPlace(Context context) {
		DatabaseHelper dbHelper = new DatabaseHelper(context);    	
		try {

			SQLiteDatabase db = dbHelper.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put("reference", this.reference);
			values.put("name", this.name);
			values.put("address", this.address);
			values.put("latitude", this.latitude);
			values.put("longitude", this.longitude);
			values.put("photoURL", this.photoURL);			
			values.put("createdAt", getDateTime());

			// insert row
			db.insert(DatabaseHelper.TABLE_PLACES, null, values);
			return true;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
		finally {			
			dbHelper.closeDB();
		}

	}

	/**
	 * Get single place
	 */
	public PlaceModel getPlaceByReference(Context context,String reference) {
		DatabaseHelper dbHelper = new DatabaseHelper(context);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		PlaceModel placeModel = null;

		String selectQuery = "SELECT  * FROM " + DatabaseHelper.TABLE_PLACES + 
				" WHERE reference = '" + reference+"'";

		Log.e("PlaceModel", selectQuery);

		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor != null && cursor.moveToFirst())
		{
			placeModel = new PlaceModel();
			placeModel.id = (cursor.getInt(cursor.getColumnIndex("id")));
			placeModel.reference = (cursor.getString(cursor.getColumnIndex("reference")));
			placeModel.name = ((cursor.getString(cursor.getColumnIndex("name"))));
			placeModel.address = ((cursor.getString(cursor.getColumnIndex("address"))));
			placeModel.latitude = ((cursor.getString(cursor.getColumnIndex("latitude"))));
			placeModel.longitude = ((cursor.getString(cursor.getColumnIndex("longitude"))));
			placeModel.photoURL = ((cursor.getString(cursor.getColumnIndex("photoURL"))));

		}            
		dbHelper.closeDB();
		return placeModel;
	}

	/**
	 * Get all places
	 */
	public ArrayList<PlaceModel> getAllPlaces(Context context) {
		DatabaseHelper dbHelper = new DatabaseHelper(context);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		ArrayList<PlaceModel> lstPlaces = new ArrayList<PlaceModel>();

		String selectQuery = "SELECT  * FROM " + DatabaseHelper.TABLE_PLACES +
				" ORDER BY id DESC"; 

		Cursor cursor = db.rawQuery(selectQuery, null);


		if (cursor != null && cursor.moveToFirst())
		{

			do {
				PlaceModel placeModel = new PlaceModel();
				placeModel.id = (cursor.getInt(cursor.getColumnIndex("id")));
				placeModel.reference = (cursor.getString(cursor.getColumnIndex("reference")));
				placeModel.name = ((cursor.getString(cursor.getColumnIndex("name"))));
				placeModel.address = ((cursor.getString(cursor.getColumnIndex("address"))));
				placeModel.latitude = ((cursor.getString(cursor.getColumnIndex("latitude"))));
				placeModel.longitude = ((cursor.getString(cursor.getColumnIndex("longitude"))));
				placeModel.photoURL = ((cursor.getString(cursor.getColumnIndex("photoURL"))));

				lstPlaces.add(placeModel);
			}while(cursor.moveToNext());


		}            
		dbHelper.closeDB();
		return lstPlaces;
	}

	/*
	 * Deleting a favorite places
	 */
	public void deletePlaceByReference(String reference,Context context) {

		DatabaseHelper dbHelper = new DatabaseHelper(context); 
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		db.delete(DatabaseHelper.TABLE_PLACES, "reference = ?",
				new String[] { reference });
		dbHelper.closeDB();
	}
	/**
	 * get datetime
	 * */
	private String getDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		Date date = new Date();
		return dateFormat.format(date);
	}


}

package com.android.goplaceme;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.goplaceme.imagecache.ImageLoader;
import com.android.goplaceme.model.PlaceModel;

public class ActivityMyFavoritePlacesScreen extends Activity {

	ListView lstView_places;

	ArrayList<PlaceModel> lst_PlaceModel;

	PlaceModel objPlace;	

	FavoritePlacesAdapter favoritePlacesAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_favorite_places);

		initUI();
	}

	/**
	 * THis function will initialize all UI component
	 */
	private void initUI() {

		lstView_places = (ListView)findViewById(R.id.listViewFavPlaces);
		lstView_places.setEmptyView(findViewById(R.id.empty));
		lstView_places.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {

				PlaceModel objPlace = lst_PlaceModel.get(position);
				
				// Starting details screen 
				Intent intent_placeDetails = new Intent(ActivityMyFavoritePlacesScreen.this,ActivityPlaceDetailsScreen.class);
				intent_placeDetails.putExtra("reference", objPlace.reference);
				intent_placeDetails.putExtra("user_latitude", ActivityLandingScreen.dbl_latitude);
				intent_placeDetails.putExtra("user_longitude", ActivityLandingScreen.dbl_longitude);
				startActivity(intent_placeDetails);						
			}
		});

		lst_PlaceModel = new ArrayList<PlaceModel>();
		objPlace = new PlaceModel();
		lst_PlaceModel = objPlace.getAllPlaces(ActivityMyFavoritePlacesScreen.this);		
		favoritePlacesAdapter = new FavoritePlacesAdapter(lst_PlaceModel, ActivityMyFavoritePlacesScreen.this);
		lstView_places.setAdapter(favoritePlacesAdapter);
	}

	/**
	 * Adapter class for favorite places
	 * @author Shri
	 *
	 */
	private class FavoritePlacesAdapter extends BaseAdapter {

		private ArrayList<PlaceModel> places;
		private Activity activity;

		public FavoritePlacesAdapter(ArrayList<PlaceModel> placesContents,Activity activity) {
			this.places = placesContents;
			this.activity = activity;
		}

		@Override
		public int getCount() {
			return (places != null) ? places.size() : 0;
		}

		@Override
		public PlaceModel getItem(int position) {
			return places.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			PlacesContentViewHolder placeContentViewHolder = null;

			convertView = activity.getLayoutInflater().inflate(R.layout.item_fav_list, parent, false);

			placeContentViewHolder = new PlacesContentViewHolder();
			placeContentViewHolder.imageViewBanner = (ImageView) convertView.findViewById(R.id.imgViewBanner);
			placeContentViewHolder.txtViewName = (TextView) convertView.findViewById(R.id.txtViewName);
			placeContentViewHolder.txtViewAddress = (TextView) convertView.findViewById(R.id.txtViewAddress);

			convertView.setTag(placeContentViewHolder);

			PlaceModel placeContent = places.get(position);
			placeContentViewHolder.txtViewName.setText(placeContent.name);
			placeContentViewHolder.txtViewAddress.setText(placeContent.address);

			int loader = R.drawable.ic_default;
			ImageLoader imgLoader = new ImageLoader(getApplicationContext());	
			if(placeContent.photoURL != null && !placeContent.photoURL.equals(""))
			{
				imgLoader.DisplayImage(placeContent.photoURL, loader, placeContentViewHolder.imageViewBanner);

			}
			return convertView;
		}

		private class PlacesContentViewHolder {
			ImageView imageViewBanner;			
			TextView txtViewName;
			TextView txtViewAddress;

		}
	}
}
